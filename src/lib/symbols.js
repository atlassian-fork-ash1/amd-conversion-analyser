'use strict';

module.exports = function (useUTF8) {
    if(useUTF8) return {
        OK: "\u2713",
        ERR: "\u2717",
        ALERT: "\u26A0"
    };

    return {
        OK: "o",
        ERR: "x",
        ALERT: "!"
    };
};