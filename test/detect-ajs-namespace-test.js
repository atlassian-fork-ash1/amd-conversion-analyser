'use strict';
const chai = require("chai");
chai.should();
chai.use(require('chai-things'));

const expect = chai.expect;

describe("DetectAJSnamespace", function () {
    const detectLegacyFallback = require("../src/lib/detect-ajs-namespace");

    it("accepts source and returns an array", function () {
        const result = detectLegacyFallback("foo = 1;");
        expect(result).to.be.instanceof(Array);
    });

    describe("on AJS.namespace blocks", function () {

        it("detects legacy fallbacks for named modules", function () {
            const results = [];
            const result1 = detectLegacyFallback("AJS.namespace('window.amdModule', null, require('test/data/legacy-amd-module'));");
            const result2 = detectLegacyFallback("AJS.namespace('GH.AmdModule1', null, require('test/data/LegacyAmdModule1'));");
            results.push(result1, result2);

            results.forEach(function (r) {
                expect(r).to.be.instanceof(Array);
                expect(r).to.have.lengthOf(1);
            });

            expect(result1[0]).to.have.property('name', 'window.amdModule');
            expect(result1[0]).to.have.property('moduleName', 'test/data/legacy-amd-module');
            expect(result1[0]).to.have.property('properName', 'amdModule');
            expect(result2[0]).to.have.property('name', 'GH.AmdModule1');
            expect(result2[0]).to.have.property('moduleName', 'test/data/LegacyAmdModule1');
            expect(result2[0]).to.have.property('properName', 'AmdModule1');
        });

        it("detects multiple legacy fallback entries in same file", function () {
            var result = detectLegacyFallback(`AJS.namespace('window.amdModule', null, require('test/data/legacy-amd-module'));
                                                        AJS.namespace('window.AmdModule1', null, require('test/data/legacy-amd-module1'))`);
            expect(result[0]).to.have.property('name', 'window.amdModule');
            expect(result[0]).to.have.property('moduleName', 'test/data/legacy-amd-module');
            expect(result[0]).to.have.property('properName', 'amdModule');
            expect(result[1]).to.have.property('name', 'window.AmdModule1');
            expect(result[1]).to.have.property('moduleName', 'test/data/legacy-amd-module1');
            expect(result[1]).to.have.property('properName', 'AmdModule1');
        });

        it("ignores files without legacy fallback", function () {
            const results = [];
            const result = detectLegacyFallback("define('foo', function() {});");
            const result1 = detectLegacyFallback("require('foo', function(){});");
            const result2 = detectLegacyFallback("console.log('hello world');");
            results.concat(result, result1, result2);

            expect(results).to.have.lengthOf(0);
        });

        it("ignores NewExpression", function () {
            const result = detectLegacyFallback("AJS.namespace('window.amdModule', null, new require('test/data/legacy-amd-module'));");
            expect(result).to.have.lengthOf(0);
        });

        it("ignores MemberExpression", function () {
            const result = detectLegacyFallback("AJS.namespace('window.amdModule', null, require('test/data/legacy-amd-module').log);");
            expect(result).to.have.lengthOf(0);
        });

        it("ignores ObjectExpression", function () {
            const result = detectLegacyFallback("AJS.namespace('window.amdModule', null, {});");
            expect(result).to.have.lengthOf(0);
        });
    });
});